[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Event-related potential

- analyses not currently reported or documented

**a_stimulus_erp**

```
- create individual ERPs in A_StimulusERP/
- CSD transform
- stim-locked
- baseline: average of -500 ms to 0 ms prior to cue onset
- avg. for each condition
````