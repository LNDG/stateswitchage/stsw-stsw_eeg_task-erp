
% Plot early visual potentials and assess ERPs during stimulus processing

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'B_analyses/X1_preprocEEGData/'];
pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);

addpath([pn.tools,'fieldtrip-20170904/']); ft_defaults;

%% define IDs

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for id = 1:length(IDs)
    pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/A_StimulusERP/';
    load([pn.out, IDs{id}, '_ERP.mat'], 'dataStimAvg');
    ERPstruct.dataStimAvg(:,id) = dataStimAvg(:,1);
end

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

cfg = [];
for indAge = 1
    for indCond = 1:4
        cfg.keepindividual = 'yes';
        dataStimGrandAvg{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataStimAvg{indCond,ageIdx{indAge}});
    end
end

%% Early evoked potential

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/';
addpath([pn.root, 'T_tools/brewermap']) % add colorbrewer
addpath([pn.root, 'T_tools/shadedErrorBar']); % add shadedErrorBar
addpath([pn.root, 'T_tools/barwitherr/']); % add barwitherr
addpath([pn.root, 'T_tools/']); % add convertPtoExponential

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);

dataStimGrandAvgTotal = cat(4, dataStimGrandAvg{1,1}.individual, dataStimGrandAvg{2,1}.individual, ...
    dataStimGrandAvg{3,1}.individual, dataStimGrandAvg{4,1}.individual);

h = figure('units','normalized','position',[.1 .1 .7 .6]);
subplot(2,1,1); cla;
hold on;
    %condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],:,:),2),4));
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,1),2));
    %curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    xlim([-1000 3100])
    title('ERP magnitude increases alongside divided attention demands')
    xlabel('Time (ms from stimulus onset)'); ylabel('Amplitude (?Volt)')
	legend([l1.mainLine], {'Viusal ERP Load 1'}); legend('boxoff');
subplot(2,1,2);
%h = figure('units','normalized','position',[.1 .1 .7 .3]);
hold on;
    condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],:,2:4)-repmat(dataStimGrandAvgTotal(:,[58:60],:,1),1,1,1,3),2),4));
%     curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,1)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l1 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,2)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,3)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(:,[58:60],:,4)-dataStimGrandAvgTotal(:,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    
    % load linear regression statistics
    
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/G_stimERP_CBPA_YA.mat', 'stat', 'cfgStat')

    significantLinear = stat.mask;
    significantLinear = significantLinear*-1.5*10^-4;
    significantLinear(significantLinear==0) = NaN;
    plot(stat.time, significantLinear, 'k', 'LineWidth', 5)
    xlim([-1000 3100])
    legend([l2.mainLine, l3.mainLine, l4.mainLine], {'Load 2 - Load 1'; 'Load 3 - Load 1'; 'Load 4 - Load 1'}); legend('boxoff');
    title('ERP magnitude increases alongside divided attention demands')
    xlabel('Time (ms from stimulus onset)'); ylabel('Amplitude difference to selective attention')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/C_figures/';
figureName = 'I_stimERPmodulation';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% save individual estimates during significant time window

IndividualERP_dat = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(:,[58:60],stat.mask,:),3),2));

IndividualERP = [];
IndividualERP.data = IndividualERP_dat;
IndividualERP.IDs = IDs; 

% estimate linear effects
X = [1 1; 1 2; 1 3; 1 4]; 
b=X\IndividualERP.data'; 
IndividualERP.data_linear(:,1) = b(2,:);

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/I2_IndividualERP.mat', 'IndividualERP');

%% plot as a function of XYZ

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

sumIDs = ismember(STSWD_summary.IDs,IDs); 

%[sortVal, sortIdx] = sort(BS_meancent.data_linear, 'ascend');
%[sortVal, sortIdx] = sort(STSWD_summary.spectralslope_linearChange(sumIDs,1), 'descend');
%[sortVal, sortIdx] = sort(STSWD_summary.MSE_slope(sumIDs,1), 'ascend');
[sortVal, sortIdx] = sort(STSWD_summary.HDDM_vt.driftEEG_linear(sumIDs,1), 'ascend');
%[sortVal, sortIdx] = sort(STSWD_summary.EEG_LV1.slope(sumIDs,1), 'descend');

highChange = sortIdx(1:23);
lowChange = sortIdx(24:47);

h = figure('units','normalized','position',[.1 .1 .7 .3]);
hold on;
    condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(highChange,[58:60],:,1),2),4));    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(highChange,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', .5*cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);

    condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(highChange,[58:60],:,4),2),4));    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(highChange,[58:60],:,4),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    
     condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(lowChange,[58:60],:,1),2),4));    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(lowChange,[58:60],:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_low = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', .5*cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    
    condAvg = squeeze(nanmean(nanmean(dataStimGrandAvgTotal(lowChange,[58:60],:,4),2),4));    
    curData = squeeze(nanmean(dataStimGrandAvgTotal(lowChange,[58:60],:,4),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4_low = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    
h = figure('units','normalized','position',[.1 .1 .7 .3]);
hold on;
    curData = squeeze(nanmean(dataStimGrandAvgTotal(highChange,[58,60],:,1),2)-dataStimGrandAvgTotal(highChange,59,:,1));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(dataStimGrandAvgTotal(highChange,[58,60],:,2),2)-dataStimGrandAvgTotal(highChange,59,:,2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(dataStimGrandAvgTotal(highChange,[58,60],:,3),2)-dataStimGrandAvgTotal(highChange,59,:,3));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(dataStimGrandAvgTotal(highChange,[58,60],:,4),2)-dataStimGrandAvgTotal(highChange,59,:,4));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    
    
h = figure('units','normalized','position',[.1 .1 .7 .3]);
hold on;
    curData = squeeze(dataStimGrandAvgTotal(:,60,:,1));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(dataStimGrandAvgTotal(:,60,:,2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(dataStimGrandAvgTotal(:,60,:,3));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(dataStimGrandAvgTotal(:,60,:,4));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    
% calculate temporal residuals in lateral channels

for indChan = 1:60
    for indID = 1:47
        for indCond = 1:4
            y = squeeze([dataStimGrandAvgTotal(indID,indChan,:,indCond)]);
            y2 = squeeze([dataStimGrandAvgTotal(indID,59,:,indCond)]);
            X = [repmat(1,numel(y),1), y2];
            [~, ~, dataStimGrandAvgTotal_res(indID,indChan,:,indCond)] = regress(y,X);
        end
    end
end

h = figure('units','normalized','position',[.1 .1 .7 .3]);
hold on;
    curData = squeeze(nanmean(dataStimGrandAvgTotal_res(:,[58,60],:,1),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(dataStimGrandAvgTotal_res(:,[58,60],:,2),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(dataStimGrandAvgTotal_res(:,[58,60],:,3),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(dataStimGrandAvgTotal_res(:,[58,60],:,4),2));
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1_high = shadedErrorBar(dataStimGrandAvg{1,1}.time,nanmean(curData,1),standError, ...
        'lineprops', {'linewidth', 2}, 'patchSaturation', .25);
